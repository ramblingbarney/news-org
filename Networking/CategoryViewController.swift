
import UIKit

struct Welcome: Codable {
    let status: String
    let sources: [SourceArticle]
}

struct SourceArticle: Codable{
    let name: String
    let url: String
    let author: String
    
    enum CodingKeys: String, CodingKey {
        case author = "id"
        case name = "name"
        case url = "url"
    }
}

// MARK: - Network Manager
class NetworkManager {
    
    private let apiKey = valueForAPIKey(named:"NEWS_API")
    
    // The session to use to download the data
    private let session: URLSession
    
    // Create the manager with a given session configuration
    init(_ sessionConfiguration: URLSessionConfiguration = .ephemeral) {
        session = URLSession(configuration: sessionConfiguration)
    }
    
    // A private function to fetch data from a URL
    private func fetchData(from url: URL, completion: @escaping (Data?) -> Void) {
        
        var request = URLRequest(url: url)
        request.addValue("\(apiKey)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error)
                completion(nil)
            } else if let data = data {
                completion(data)
            }
        }
        
        dataTask.resume()
        
    }
    
    func fetchCategory(_ categoryName: String, completion: @escaping (Welcome?) -> Void) {
        
        // Create the URL Components
        guard var urlComps = URLComponents(string: "https://newsapi.org/") else {
            print("Unable to create URL Components")
            return completion(nil)
        }
        
        // Add the path
        //        urlComps.path = "/v2/top-headlines?category=\(categoryName)"
        urlComps.path = "/v1/sources"
        
        
        
        // Get the URL from the URL Components
        guard let articles = urlComps.url else {
            print("Unable to create URL")
            return completion(nil)
        }
        
        // Fetch the data
        fetchData(from: articles) { data in
            
            if let data = data {
                
                // Parse the response
                do {
                    let value = try JSONDecoder().decode(Welcome.self, from: data)
                    
                    completion(value)
                } catch {
                    print(error)
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
    
    // A function to fetch the image of the article
    func fetch(articleImage path: String, completion: @escaping (UIImage?) -> Void) {
        
        // Create a URL from the path
        guard let articleImageURL = URL(string: path) else {
            print("Unable to create URL")
            return completion(nil)
        }
        
        // Fetch the data
        fetchData(from: articleImageURL) { data in
            if let data = data {
                // Call the completion with the data
                completion(UIImage(data: data))
            } else {
                completion(nil)
            }
        }
        
    }
    
}


// MARK: - ViewController
class CategoryViewController: UIViewController {
    
    var selectedTitle = ""
    var selectedDescription = ""
    let cellId = "asfjowieurilsjflksjdflkjdsf292398329834"
    var articlesTable: UITableView!
    // The network manager
    fileprivate let networkManager = NetworkManager()
    // The data model
    fileprivate var model = Welcome(status: "model", sources: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.white
        navigationItem.title = selectedTitle + selectedDescription
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(back))
        
        setupUI()
        setUpConstraints()
        articlesTable.dataSource = self
        articlesTable.delegate = self
        articlesTable.separatorColor = UIColor.red
        articlesTable.rowHeight = UITableView.automaticDimension
        articlesTable.estimatedRowHeight = 44.0
        articlesTable.tableFooterView = UIView()
        articlesTable.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        articlesTable.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
        update()
        
    }
    
    private func setupUI() {
        articlesTable = UITableView(frame: .zero, style: .plain)
        articlesTable.separatorColor = UIColor.red
        view.addSubview(articlesTable)
    }
    
    private func setUpConstraints() {
        
        let margins = view.layoutMarginsGuide
        
        articlesTable.translatesAutoresizingMaskIntoConstraints = false
        articlesTable.topAnchor.constraint(equalTo:margins.topAnchor, constant: 20.0).isActive = true
        articlesTable.leadingAnchor.constraint(equalTo:margins.leadingAnchor).isActive = true
        articlesTable.trailingAnchor.constraint(equalTo:margins.trailingAnchor).isActive = true
        articlesTable.bottomAnchor.constraint(equalTo:margins.bottomAnchor, constant: -20.0).isActive = true
        view.addSubview(articlesTable)
    }
    
    
    @objc func back() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    // When we update we fetch 40 random images then we reload the CollectionView
    func update() {
        networkManager.fetchCategory(selectedTitle.lowercased()) { response in
            if let response = response {
                self.model = response
                DispatchQueue.main.async {
                    self.articlesTable.reloadData()
                }
            }
        }
    }
}

extension CategoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current

        formatter.dateFormat = "yyyy-MM-dd"

        let dateString = formatter.string(from: now)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        // no image url available so this is a hardcoded replacement to demonstrate functionality
        networkManager.fetch(articleImage: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Whois_icon.png/120px-Whois_icon.png") { image in
            DispatchQueue.main.async {
                cell.imageView?.image = image
            }
        }
        
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = model.sources[indexPath.row].name
        cell.detailTextLabel?.text = model.sources[indexPath.row].author + " \(dateString)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let vc = WebViewViewController()
        vc.webURL = model.sources[indexPath.row].url
        vc.selectedTitle = model.sources[indexPath.row].name
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

