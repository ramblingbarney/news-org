//  Created by The App Experts on 23/10/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit
import Foundation

struct Root: Codable {
    let sources: [Source]
    let status: String
}

struct Source: Codable{
    let category: String
    let sortBysAvailable: [String]
}

class SectionModel {
    
    weak var delegate: DataReloadTableViewDelegate?
    
    var sections: [String]
    
    // The session to use to download the data
    private let session: URLSession
    
    // Create the manager with a given session configuration
    init(_ sessionConfiguration: URLSessionConfiguration = .ephemeral) {
        session = URLSession(configuration: sessionConfiguration)
        sections = []
    }
    
    // unction to fetch data from a URL
    func fetchData() -> Void {
        
        // Create the URL Components
        guard var urlComps = URLComponents(string: "https://newsapi.org/") else {
            print("Unable to create URL Components")
            return
        }
        
        // Add the path
        urlComps.path = "/v1/sources"
        
        // Get the URL from the URL Components
        guard let allSections = urlComps.url else {
            print("Unable to create URL")
            return
        }
        
        let dataTask = session.dataTask(with: allSections) { (data, response, error) in
            if let error = error {
                print(error)
            } else if let data = data {
                
                // Parse the response
                do {
                    let value = try JSONDecoder().decode(Root.self, from: data)
                    
                    if value.status == "ok" {
                        
                        self.sections.removeAll()
                        
                        var grossSections = [String]()
                        
                        for (element) in value.sources.enumerated() {
                            if element.element.sortBysAvailable.contains("top") {
                                grossSections.append(element.element.category)
                            }
                        }
                        
                        var uniqueList = [String]()
                        
                        for element in Array(Set(grossSections)).enumerated() {
                            uniqueList.append(element.element.capitalized)
                        }
                        
                        self.sections = uniqueList.sorted { $0 < $1 }
                        self.delegate?.reloadNetworkTable()
                    }
                } catch {
                    print(error)
                }
                
            }
            
        }
        dataTask.resume()
        
    }
    
}
