//
//  AppDelegate.swift
//  Networking
//
//  Created by Brian @ TAE on 11/10/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let listTable = ViewController()
        
        let navigationController = UINavigationController(rootViewController: listTable)
        navigationController.navigationBar.barTintColor = UIColor.red
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }

   
}

