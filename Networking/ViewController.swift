//
//  ViewController.swift
//  DataPersistanceWalkThrough
//
//  Created by The App Experts on 21/10/2019.
//  Copyright © 2019 Conor O'Dwyer. All rights reserved.
//

import UIKit

protocol DataReloadTableViewDelegate: class{
    func reloadNetworkTable()
}


class ViewController: UIViewController {
    
    let cellId = "asfjowieurilsjflksjdflkjdsf292398329834"
    var model: SectionModel!
    var categoriesTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.white
        model = SectionModel()
        model.delegate = self
        
        navigationItem.title = "News ORG"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refresh))
        setupUI()
        setUpConstraints()
        categoriesTable.dataSource = self
        categoriesTable.delegate = self
        categoriesTable.separatorColor = UIColor.red
        categoriesTable.rowHeight = UITableView.automaticDimension
        categoriesTable.estimatedRowHeight = 44.0
        categoriesTable.tableFooterView = UIView()
        categoriesTable.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        categoriesTable.register(SubtitleTableViewCell.self, forCellReuseIdentifier: cellId)
        model.fetchData()
    }
    
    private func setupUI() {
        
        categoriesTable = UITableView(frame: .zero, style: .plain)
        categoriesTable.separatorColor = UIColor.red
        view.addSubview(categoriesTable)
    }
    
    private func setUpConstraints() {
        
        let margins = view.layoutMarginsGuide
        
        categoriesTable.translatesAutoresizingMaskIntoConstraints = false
        categoriesTable.topAnchor.constraint(equalTo:margins.topAnchor, constant: 20.0).isActive = true
        categoriesTable.leadingAnchor.constraint(equalTo:margins.leadingAnchor).isActive = true
        categoriesTable.trailingAnchor.constraint(equalTo:margins.trailingAnchor).isActive = true
        categoriesTable.bottomAnchor.constraint(equalTo:margins.bottomAnchor, constant: -20.0).isActive = true
        view.addSubview(categoriesTable)
    }
    
    
    
    
    @objc func refresh() -> Void {
        model.fetchData()
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = model.sections[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath)
        guard let titleText = currentCell?.textLabel!.text else { return }
        let vc = CategoryViewController()
        vc.selectedTitle = titleText
        vc.selectedDescription = " Top Articles"
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

class SubtitleTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ViewController: DataReloadTableViewDelegate{
    
    func reloadNetworkTable(){
        DispatchQueue.main.async {
            self.categoriesTable.reloadData()
        }
    }
    
}

