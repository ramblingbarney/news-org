//
//  WebViewViewController.swift
//  Networking
//
//  Created by The App Experts on 27/10/2019.
//  Copyright © 2019 Test. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKUIDelegate {
    
    var webURL = ""
    var selectedTitle = ""
    var webView: WKWebView!

    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
                navigationItem.title = selectedTitle
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        let myURL = URL(string: webURL)
         let myRequest = URLRequest(url: myURL!)
         webView.load(myRequest)

    }
    

    
    
    @objc func cancel() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
}
